import os

from conans import ConanFile, CMake

project_name = "gfx3d_templates"


class Gfx3dConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    license = "MIT"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/%s" % project_name
    description = "3D primitive template generators"
    topics = ("asd", project_name)
    generators = "cmake"
    exports_sources = "include*", "src*", "CMakeLists.txt", "asd.json"
    requires = (
        "asd.gfx/0.0.1@asd/testing"
    )

    def source(self):
        pass

    def build(self):
        cmake = CMake(self)
        cmake.definitions['CMAKE_MODULE_PATH'] = self.deps_user_info["asd.build_tools"].module_path
        cmake.configure()

    def package(self):
        self.copy("*.h", dst="include", src="include")

    def package_info(self):
        self.info.header_only()
