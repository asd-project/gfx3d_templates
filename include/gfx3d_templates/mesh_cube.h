//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gfx3d_templates/mesh_factory.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        namespace detail
        {
            template <class Layout>
            struct mesh_cube {};

            template <class Layout>
            struct mesh_cube_frame {};
        }

        template <class Layout>
        struct mesh_cube_factory : mesh_factory<detail::mesh_cube<Layout>, Layout> {};

        template <class Layout>
        struct mesh_cube_frame_factory : mesh_factory<detail::mesh_cube_frame<Layout>, Layout> {};

        namespace detail
        {
            constexpr std::array short_cube_vertices = {
                std::array{-1.0f, +1.0f, -1.0f}, // ltf 0
                std::array{-1.0f, -1.0f, -1.0f}, // lbf 1
                std::array{+1.0f, -1.0f, -1.0f}, // rbf 2
                std::array{+1.0f, +1.0f, -1.0f}, // rtf 3
                std::array{-1.0f, +1.0f, +1.0f}, // ltb 4
                std::array{-1.0f, -1.0f, +1.0f}, // lbb 5
                std::array{+1.0f, -1.0f, +1.0f}, // rbb 6
                std::array{+1.0f, +1.0f, +1.0f}, // rtb 7
            };

            constexpr std::array cube_vertices = {
                                                 // left
                std::array{-1.0f, +1.0f, +1.0f}, // ltb 8
                std::array{-1.0f, -1.0f, +1.0f}, // lbb 9
                std::array{-1.0f, -1.0f, -1.0f}, // lbf 10
                std::array{-1.0f, +1.0f, -1.0f}, // ltf 11

                                                 // bottom
                std::array{-1.0f, -1.0f, -1.0f}, // lbf 16
                std::array{-1.0f, -1.0f, +1.0f}, // lbb 17
                std::array{+1.0f, -1.0f, +1.0f}, // rbb 18
                std::array{+1.0f, -1.0f, -1.0f}, // rbf 19

                                                 // front
                std::array{-1.0f, +1.0f, -1.0f}, // ltf 0
                std::array{-1.0f, -1.0f, -1.0f}, // lbf 1
                std::array{+1.0f, -1.0f, -1.0f}, // rbf 2
                std::array{+1.0f, +1.0f, -1.0f}, // rtf 3

                                                 // right
                std::array{+1.0f, +1.0f, -1.0f}, // rtf 12
                std::array{+1.0f, -1.0f, -1.0f}, // rbf 13
                std::array{+1.0f, -1.0f, +1.0f}, // rbb 14
                std::array{+1.0f, +1.0f, +1.0f}, // rtb 15

                                                 // top
                std::array{-1.0f, +1.0f, +1.0f}, // ltb 20
                std::array{-1.0f, +1.0f, -1.0f}, // ltf 21
                std::array{+1.0f, +1.0f, -1.0f}, // rtf 22
                std::array{+1.0f, +1.0f, +1.0f}, // rtb 23

                                                 // back
                std::array{+1.0f, +1.0f, +1.0f}, // rtb 4
                std::array{+1.0f, -1.0f, +1.0f}, // rbb 5
                std::array{-1.0f, -1.0f, +1.0f}, // lbb 6
                std::array{-1.0f, +1.0f, +1.0f}, // ltb 7
            };

            constexpr std::array<u8, 36> cube_indices = {
                0,   1,  2,  2,  3,  0, // left
                4,   5,  6,  6,  7,  4, // bottom
                8,   9, 10, 10, 11,  8, // front
                12, 13, 14, 14, 15, 12, // right
                16, 17, 18, 18, 19, 16, // top
                20, 21, 22, 22, 23, 20, // back
            };

            constexpr std::array side_texcoord2 = {
                std::array{+0.0f, +0.0f},
                std::array{+0.0f, +1.0f},
                std::array{+1.0f, +1.0f},
                std::array{+1.0f, +0.0f},
            };

            constexpr std::array side_texcoord3 = {
                std::array{+0.0f, +0.0f, +0.0f},
                std::array{+0.0f, +1.0f, +0.0f},
                std::array{+1.0f, +1.0f, +0.0f},
                std::array{+1.0f, +0.0f, +0.0f},
            };

            constexpr std::array cube_normals = {
                std::array{-1.0f, +0.0f, +0.0f}, // left
                std::array{+0.0f, -1.0f, +0.0f}, // bottom
                std::array{+0.0f, +0.0f, -1.0f}, // front
                std::array{+1.0f, +0.0f, +0.0f}, // right
                std::array{+0.0f, +1.0f, +0.0f}, // top
                std::array{+0.0f, +0.0f, +1.0f}, // back
            };

            constexpr std::array<u8, 48> cube_frame_indices = {
                0,   1,  1,  2,  2,  3,  3,  0, // front
                4,   5,  5,  6,  6,  7,  7,  4, // back
                8,   9,  9, 10, 10, 11, 11,  8, // left
                12, 13, 13, 14, 14, 15, 15, 12, // right
                16, 17, 17, 18, 18, 19, 19, 16, // bottom
                20, 21, 21, 22, 22, 23, 23, 20, // top
            };

            template <class Element>
            struct mesh_cube_vertex_element {};

            template <>
            struct mesh_cube_vertex_element<vertex_attributes::p3>
            {
                static constexpr auto data = cube_vertices;
            };
            template <>
            struct mesh_cube_vertex_element<vertex_attributes::n3>
            {
                static constexpr auto data = repeat_each<4>(cube_normals);
            };
            template <>
            struct mesh_cube_vertex_element<vertex_attributes::t2>
            {
                static constexpr auto data = repeat_all<6>(side_texcoord2);
            };
            template <>
            struct mesh_cube_vertex_element<vertex_attributes::t3>
            {
                static constexpr auto data = repeat_all<6>(side_texcoord3);
            };

            template <class ... E>
            struct mesh_cube<vertex_layouts::generator<E...>>
            {
                static constexpr auto primitives_type = gfx::primitives_type::triangles;
                static constexpr auto vertices = zip(mesh_cube_vertex_element<E>::data...);
                static constexpr auto indices = cube_indices;
            };

            template <class ... E>
            struct mesh_cube_frame<vertex_layouts::generator<E...>>
            {
                static constexpr auto primitives_type = gfx::primitives_type::lines;
                static constexpr auto vertices = zip(mesh_cube_vertex_element<E>::data...);
                static constexpr auto indices = cube_frame_indices;
            };
        }
    }
}
