//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gfx3d_templates/mesh_factory.h>

#include <gcem.hpp>
#include <boost/integer.hpp>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        namespace detail
        {
            template <size_t Subdivisions, class Layout>
            struct mesh_ellipse {};
        }

        template <size_t Subdivisions, class Layout>
        struct mesh_ellipse_factory : mesh_factory<detail::mesh_ellipse<Subdivisions, Layout>, Layout>
        {};

        namespace detail
        {
            template <size_t Subdivisions, size_t Units, class Initializer, class Generator>
            constexpr array<float, (Subdivisions + 1) * Units> ellipse_vertices(Initializer initializer, Generator generator) {
                array<float, (Subdivisions + 1) * Units> vertices {};

                initializer(vertices);

                for (size_t i = 0; i < Subdivisions; ++i) {
                    generator(vertices, (i + 1) * Units, static_cast<float>(2 * i * GCEM_PI / Subdivisions));
                }

                return vertices;
            }

            template <size_t Subdivisions, class IndexType = typename boost::uint_value_t<Subdivisions>::least>
            constexpr array<IndexType, (Subdivisions + 1) * 3> ellipse_indices() {
                array<IndexType, (Subdivisions + 1) * 3> indices {};

                for (size_t i = 0; i < Subdivisions - 1; ++i) {
                    indices[i * 3] = 0;
                    indices[i * 3 + 1] = static_cast<IndexType>(i + 1);
                    indices[i * 3 + 2] = static_cast<IndexType>(i + 2);
                }

                indices[Subdivisions * 3] = 0;
                indices[Subdivisions * 3 + 1] = static_cast<IndexType>(Subdivisions);
                indices[Subdivisions * 3 + 2] = 1;

                return indices;
            }

            template <size_t Subdivisions>
            struct mesh_ellipse<Subdivisions, vertex_layouts::p2>
            {
                static constexpr auto primitives_type = gfx::primitives_type::triangles;

                static constexpr auto vertices = ellipse_vertices<Subdivisions, 2>([](auto & vertices) constexpr {
                    vertices[0] = 0.0f;
                    vertices[1] = 0.0f;
                }, [](auto & vertices, size_t index, auto angle) constexpr {
                    vertices[index] = gcem::cos(angle);
                    vertices[index + 1] = gcem::sin(angle);
                });

                static constexpr auto indices = ellipse_indices<Subdivisions>();
            };

            template <size_t Subdivisions>
            struct mesh_ellipse<Subdivisions, vertex_layouts::p2n2>
            {
                static constexpr auto primitives_type = gfx::primitives_type::triangles;

                static constexpr std::array vertices = {
                    -1.0f, +1.0f, 0.0f, 0.0f, 1.0f, // lt 0
                    -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, // lb 1
                    +1.0f, -1.0f, 0.0f, 0.0f, 1.0f, // rb 2
                    +1.0f, +1.0f, 0.0f, 0.0f, 1.0f, // rt 3
                };

                static constexpr std::array<u8, 6> indices = {
                    0, 1, 2,
                    2, 3, 0
                };
            };

            template <size_t Subdivisions>
            struct mesh_ellipse<Subdivisions, vertex_layouts::p2t2>
            {
                static constexpr auto primitives_type = gfx::primitives_type::triangles;

                static constexpr std::array vertices = {
                    -1.0f, +1.0f, 0.0f, 0.0f, // lt 0
                    -1.0f, -1.0f, 0.0f, 1.0f, // lb 1
                    +1.0f, -1.0f, 1.0f, 1.0f, // rb 2
                    +1.0f, +1.0f, 1.0f, 0.0f, // rt 3
                };

                static constexpr std::array<u8, 6> indices = {
                    0, 1, 2,
                    2, 3, 0
                };
            };

            template <size_t Subdivisions>
            struct mesh_ellipse<Subdivisions, vertex_layouts::p3>
            {
                static constexpr auto primitives_type = gfx::primitives_type::triangles;

                static constexpr std::array vertices = {
                    -1.0f, +1.0f, 0.0f, // lt 0
                    -1.0f, -1.0f, 0.0f, // lb 1
                    +1.0f, -1.0f, 0.0f, // rb 2
                    +1.0f, +1.0f, 0.0f, // rt 3
                };

                static constexpr std::array<u8, 6> indices = {
                    0, 1, 2,
                    2, 3, 0
                };
            };

            template <size_t Subdivisions>
            struct mesh_ellipse<Subdivisions, vertex_layouts::p3n3>
            {
                static constexpr auto primitives_type = gfx::primitives_type::triangles;

                static constexpr std::array vertices = {
                    -1.0f, +1.0f, 0.0f, 0.0f, 0.0f, 1.0f, // lt 0
                    -1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, // lb 1
                    +1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, // rb 2
                    +1.0f, +1.0f, 0.0f, 0.0f, 0.0f, 1.0f, // rt 3
                };

                static constexpr std::array<u8, 6> indices = {
                    0, 1, 2,
                    2, 3, 0
                };
            };

            template <size_t Subdivisions>
            struct mesh_ellipse<Subdivisions, vertex_layouts::p3t2>
            {
                static constexpr auto primitives_type = gfx::primitives_type::triangles;

                static constexpr std::array vertices = {
                    -1.0f, +1.0f, 0.0f, 0.0f, 0.0f, // lt 0
                    -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, // lb 1
                    +1.0f, -1.0f, 0.0f, 1.0f, 1.0f, // rb 2
                    +1.0f, +1.0f, 0.0f, 1.0f, 0.0f, // rt 3
                };

                static constexpr std::array<u8, 6> indices = {
                    0, 1, 2,
                    2, 3, 0
                };
            };
        }

    }
}
