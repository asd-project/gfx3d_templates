//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gfx/mesh.h>
#include <gfx/multimesh.h>
#include <container/plain_tuple.h>
#include <meta/tuple/unpack.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        template <size_t Index, class T, size_t N, class Transform>
        constexpr auto transform_vertex(const std::array<T, N> & vertices, Transform && transform) {
            if constexpr (meta::tuple_like<T>) {
                return meta::unpack(vertices[Index], [&](const auto & ... args) { return transform(Index, args...); });
            } else {
                return transform(Index, vertices[Index]);
            }
        }

        template <class T, size_t N, class Transform, size_t ... Indices>
        constexpr auto transform_vertices(const std::array<T, N> & vertices, Transform && transform, std::index_sequence<Indices...>) {
            return std::array { transform_vertex<Indices>(vertices, transform)... };
        }

        template <class T, size_t N, class Transform>
        constexpr auto transform_vertices(const std::array<T, N> & vertices, Transform && transform) {
            return transform_vertices(vertices, transform, std::make_index_sequence<N>{});
        }

        template <class Impl, class Layout>
        struct mesh_factory
        {
            static constexpr auto & layout = Layout::instance;
            static constexpr auto & primitives_type = Impl::primitives_type;
            static constexpr auto & vertices = Impl::vertices;
            static constexpr auto & indices = Impl::indices;

            static_assert(sizeof(Impl::vertices) % layout.size_in_bytes() == 0, "Vertex data has incorrect layout");

            template <class Gfx>
            static gfx::handle<gfx::mesh<Gfx>> create(gfx::mesh_registry<Gfx> & meshes) {
                auto mesh = meshes.create({layout, Impl::primitives_type});
                mesh->add_vertex_buffer(layout, Impl::vertices);
                mesh->set_indices(Impl::indices);

                return mesh;
            }

            template <class InstanceLayout, class Gfx>
            static gfx::handle<gfx::multimesh_scheme<Gfx>> create(gfx::multimesh_registry<Gfx> & meshes) {
                auto mesh = meshes.create({layout, InstanceLayout::instance, Impl::primitives_type});
                mesh->add_vertex_buffer(layout, Impl::vertices);
                mesh->set_indices(Impl::indices);

                return mesh;
            }

            template <class Gfx, class Transform>
            static gfx::handle<gfx::mesh<Gfx>> create(gfx::mesh_registry<Gfx> & meshes, Transform transform) {
                auto vertices = transform_vertices(Impl::vertices, transform);

                auto mesh = meshes.create({layout, Impl::primitives_type});
                mesh->add_vertex_buffer(layout, vertices);
                mesh->set_indices(Impl::indices);

                return mesh;
            }

            template <class InstanceLayout, class Gfx, class Transform>
            static gfx::handle<gfx::multimesh_scheme<Gfx>> create(gfx::multimesh_registry<Gfx> & meshes, Transform transform) {
                auto vertices = transform_vertices(Impl::vertices, transform);

                auto mesh = meshes.create({layout, InstanceLayout::instance, Impl::primitives_type});
                mesh->add_vertex_buffer(layout, vertices);
                mesh->set_indices(Impl::indices);

                return mesh;
            }
        };


        template <size_t Index, class ... A>
        constexpr auto combine(A && ... a) {
            return make_plain_tuple(get<Index>(std::forward<A>(a))...);
        }

        template <class ... A, size_t ... Indices>
        constexpr auto zip_iterate(std::index_sequence<Indices...>, A && ... a) {
            return std::array {combine<Indices>(std::forward<A>(a)...)...};
        }

        template <class ... A>
        constexpr auto zip(A && ... a) {
            constexpr size_t N = std::tuple_size_v<plain<meta::first_t<std::tuple<A...>>>>;
            static_assert(((std::tuple_size_v<std::decay_t<A>> == N) && ...), "Zipped arrays should have equal lengths");

            return zip_iterate(std::make_index_sequence<N>{}, std::forward<A>(a)...);
        }

        // repeat contents of `a` `Times` times
        // e.g. e1, e2, e3, e1, e2, e3, e1, e2, e3...
        template <size_t Times, class T, size_t N>
            requires (Times > 0)
        constexpr std::array<T, N * Times> repeat_all(const std::array<T, N> & a) {
            std::array<T, N * Times> result {};

            for (size_t index = 0; index < result.size(); ++index) {
                result[index] = a[index % N];
            }

            return result;
        }

        // repeat contents of `a` `Times` times applying transform which uses iteration index
        // e.g. f(e1, 1), f(e2, 1), f(e3, 1), f(e1, 2), f(e2, 2), f(e3, 2), f(e1, 3), f(e2, 3), f(e3, 3)...
        template <size_t Times, class T, size_t N, class F, class R = decltype(std::declval<F>()(std::declval<T>(), std::declval<size_t>()))>
            requires (Times > 0 && std::is_invocable_v<F, T, size_t>)
        constexpr std::array<R, N * Times> repeat_all(const std::array<T, N> & a, F f) {
            std::array<R, N * Times> result {};

            for (size_t index = 0; index < N * Times; ++index) {
                result[index] = f(a[index % N], index / N);
            }

            return result;
        }

        // repeat each element of `a` `Times` times consequentially
        // e.g. e1, e1, e1, e2, e2, e2, e3, e3, e3...
        template <size_t Times, class T, size_t N>
            requires (Times > 0)
        constexpr std::array<T, N * Times> repeat_each(const std::array<T, N> & a) {
            std::array<T, N * Times> result {};

            for (size_t index = 0; index < N * Times; ++index) {
                result[index] = a[index / Times];
            }

            return result;
        }
    }
}
