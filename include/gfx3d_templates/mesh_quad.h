//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gfx3d_templates/mesh_factory.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        namespace detail
        {
            template <class Layout>
            struct mesh_quad
            {};
        }

        template <class Layout>
        struct mesh_quad_factory : mesh_factory<detail::mesh_quad<Layout>, Layout>
        {};

        constexpr std::array quad_positions2 = {
            std::array{-1.0f, +1.0f}, // lt 0
            std::array{-1.0f, -1.0f}, // lb 1
            std::array{+1.0f, -1.0f}, // rb 2
            std::array{+1.0f, +1.0f}, // rt 3
        };

        constexpr std::array quad_positions3 = {
            std::array{-1.0f, +1.0f, 0.0f}, // lt 0
            std::array{-1.0f, -1.0f, 0.0f}, // lb 1
            std::array{+1.0f, -1.0f, 0.0f}, // rb 2
            std::array{+1.0f, +1.0f, 0.0f}, // rt 3
        };

        constexpr std::array<std::array<float, 2>, 1> quad_normal2 = {
            std::array{0.0f, 0.0f}
        };

        constexpr std::array<std::array<float, 3>, 1> quad_normal3 = {
            std::array{0.0f, 0.0f, 1.0f}
        };

        constexpr std::array quad_texcoord2 = {
            std::array{+0.0f, +0.0f},
            std::array{+0.0f, +1.0f},
            std::array{+1.0f, +1.0f},
            std::array{+1.0f, +0.0f},
        };

        constexpr std::array quad_texcoord3 = {
            std::array{+0.0f, +0.0f},
            std::array{+0.0f, +1.0f},
            std::array{+1.0f, +1.0f},
            std::array{+1.0f, +0.0f},
        };

        constexpr std::array<u8, 6> quad_indices = {
            0, 1, 2,
            2, 3, 0
        };

        namespace detail
        {
            template <class Element>
            struct mesh_quad_vertex_element {};

            template <>
            struct mesh_quad_vertex_element<vertex_attributes::p2>
            {
                static constexpr auto data = quad_positions2;
            };
            template <>
            struct mesh_quad_vertex_element<vertex_attributes::p3>
            {
                static constexpr auto data = quad_positions3;
            };
            template <>
            struct mesh_quad_vertex_element<vertex_attributes::n2>
            {
                static constexpr auto data = repeat_all<4>(quad_normal2);
            };
            template <>
            struct mesh_quad_vertex_element<vertex_attributes::n3>
            {
                static constexpr auto data = repeat_all<4>(quad_normal3);
            };
            template <>
            struct mesh_quad_vertex_element<vertex_attributes::t2>
            {
                static constexpr auto data = quad_texcoord2;
            };
            template <>
            struct mesh_quad_vertex_element<vertex_attributes::t3>
            {
                static constexpr auto data = quad_texcoord3;
            };

            template <class ... E>
            struct mesh_quad<vertex_layouts::generator<E...>>
            {
                static constexpr auto primitives_type = gfx::primitives_type::triangles;
                static constexpr auto vertices = zip(mesh_quad_vertex_element<E>::data...);
                static constexpr auto indices = quad_indices;
            };
        }
    }
}
