//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gfx/mesh.h>
#include <container/plain_tuple.h>
#include <meta/tuple/unpack.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        namespace transform
        {
            constexpr auto make_scaling = [](auto value) {
                return [=](size_t, auto & pos, auto & ... others) {
                    constexpr auto pos_size = std::tuple_size_v<plaintype(pos)>;
                    static_assert(pos_size != 2 && pos_size != 2, "incorrect position value");

                    if constexpr (pos_size == 2) {
                        return make_plain_tuple(std::array{ get<0>(pos) * value, get<1>(pos) * value}, others...);
                    } else if constexpr (pos_size == 3) {
                        return make_plain_tuple(std::array{ get<0>(pos) * value, get<1>(pos) * value, get<2>(pos) * value }, others...);
                    }
                };
            };

            constexpr auto make_translation = [](const auto & offset) {
                return [&](size_t, auto & pos, auto & ... others) {
                    constexpr auto pos_size = std::tuple_size_v<plaintype(pos)>;
                    static_assert(pos_size != 2 && pos_size != 2, "incorrect position value");

                    if constexpr (math::is_scalar_v<plaintype(offset)>) {
                        if constexpr (pos_size == 2) {
                            return make_plain_tuple(std::array{ get<0>(pos) + offset, get<1>(pos) + offset }, others...);
                        } else if constexpr (pos_size == 3) {
                            return make_plain_tuple(std::array{ get<0>(pos) + offset, get<1>(pos) + offset, get<2>(pos) + offset }, others...);
                        }
                    } else {
                        static_assert(std::tuple_size_v<plaintype(offset)> >= pos_size, "incorrect offset value");

                        if constexpr (pos_size == 2) {
                            return make_plain_tuple(std::array{ get<0>(pos) + offset[0], get<1>(pos) + offset[1] }, others...);
                        } else if constexpr (pos_size == 3) {
                            return make_plain_tuple(std::array{ get<0>(pos) + offset[0], get<1>(pos) + offset[1], get<2>(pos) + offset[2] }, others...);
                        }
                    }
                };
            };

            constexpr auto center_minimum = [](size_t, auto & pos, auto & ... others) {
                constexpr auto pos_size = std::tuple_size_v<plaintype(pos)>;

                if constexpr (pos_size == 2) {
                    return make_plain_tuple(std::array{ get<0>(pos) * 0.5f + 0.5f, get<1>(pos) * 0.5f + 0.5f }, others...);
                } else if constexpr (pos_size == 3) {
                    return make_plain_tuple(std::array{ get<0>(pos) * 0.5f + 0.5f, get<1>(pos) * 0.5f + 0.5f, get<2>(pos) * 0.5f + 0.5f }, others...);
                }
            };

            constexpr auto center_horizontally = [](size_t, f32v3 & pos, auto & ... others) {
                return make_plain_tuple(std::array{ get<0>(pos) * 0.5f, get<1>(pos) * 0.5f + 0.5f, get<2>(pos) * 0.5f }, others...);
            };

            template <class ... T>
            constexpr auto transform_combination(size_t idx, const plain_tuple<T...> & t) {
                return t;
            }

            template <class ... T, class H, class ... F>
            constexpr auto transform_combination(size_t idx, const plain_tuple<T...> & t, H && head, F && ... f) {
                return transform_combination(idx, meta::unpack(t, [&](const auto & ... args) {
                    return std::forward<H>(head)(idx, args...);
                }), std::forward<F>(f)...);
            }

            template <class H, class ... F>
            constexpr auto combine(H && head, F && ... f) {
                return [&](size_t idx, auto & ... args) {
                    return transform_combination(idx, std::forward<H>(head)(idx, args...), std::forward<F>(f)...);
                };
            };
        }
    }
}
